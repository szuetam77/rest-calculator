package pl.kurs.java.calculator.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kurs.java.calculator.error.ValidOperation;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalculatorRequest {



    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal firstNumber;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal secondNumber;
    @ValidOperation
    private String operator;


}
